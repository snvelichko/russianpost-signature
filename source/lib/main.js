var _ = require("sdk/l10n").get;
var prefs = require('sdk/preferences/service');
var self = require('sdk/self');

var addonPrefs = 'extensions.' + self.id

function changeSignature(){
    //Load template
    var template = self.data.load('template.html');

    //Set signature
    var accounts = prefs.get('mail.accountmanager.accounts').split(',');
    for (var i in accounts){
        var account=accounts[i];
        if (prefs.isSet('mail.account.' + account + '.identities')){
            var signature = template;
            var identities=prefs.get('mail.account.' + account + '.identities');
            signature = signature.replace('%fullname%', prefs.get('mail.identity.' + identities + '.fullName', '') , 'gi');
            signature = signature.replace('%organization%', prefs.get('mail.identity.' + identities + '.organization', '') , 'gi');
            signature = signature.replace('%useremail%', prefs.get('mail.identity.' + identities + '.useremail', '') , 'gi');

            signature = signature.replace('%position%', prefs.get(addonPrefs + '.position', '') , 'gi');
            signature = signature.replace('%departament%', prefs.get(addonPrefs + '.departament', '') , 'gi');
            signature = signature.replace('%address%', prefs.get(addonPrefs + '.address', '') , 'gi');

            if (prefs.get(addonPrefs + '.phone', '') != '')
                signature = signature.replace('%phone%', _("phonePrefix") + ' ' + prefs.get(addonPrefs + '.phone', '') , 'gi');
            else
                signature = signature.replace('%phone%', '', 'gi');

            if (prefs.get(addonPrefs + '.mobile', '') != '')
                signature = signature.replace('%mobile%', _("mobilePrefix") + ' ' + prefs.get(addonPrefs + '.mobile', '') , 'gi');
            else
                signature = signature.replace('%mobile%', '', 'gi');

            prefs.set("mail.identity." + identities + ".attach_vcard", false);
            prefs.set("mail.identity." + identities + ".attach_signature", false);
            prefs.set("mail.identity." + identities + ".htmlSigFormat", true);
            prefs.set("mail.identity." + identities + ".htmlSigText", signature);
        }
    }
}

function onPrefChange(prefName) {
    changeSignature();
}

require("sdk/simple-prefs").on("", onPrefChange);
changeSignature();
