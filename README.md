НАСТРОЙКА
---------------
Информация по установке и настройке находится в каталоге docs

РАЗРАБОТКА
---------------

1. Для сборки необходимо установить Add-on SDK (https://developer.mozilla.org/en-US/Add-ons/SDK/Tutorials/Installation)
2. Подкинуть "additional/install.rdf" в каталог "<каталог с установленным addon-sdk>/app-extension/install.rdf" (не нашёл другого варианта)
3. Для тестирования модуля запустить "run"
4. Для сборки пакета запустить "xpi"